---
title: moxa-it87-gpio-driver
---

## Moxa IT87 GPIO Driver

`moxa-it87-gpio-driver`: GPIO interface for IT87xx Super I/O chips

### Support Models

- DA Series: [DA-820C](manuals/products/DA/DA-820C.md)|[DA-682C](manuals/products/DA/DA-682C.md)|[DA-681C](manuals/products/DA/DA-681C.md)
- MC Series: [MC-3201](manuals/products/MC/MC-3201.md)

### Source Code Link

[moxa-it87-gpio-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-gpio-driver/-/tree/master)

### What Tools Depend On?

[mx-uart-ctl](manuals/tools/mx-uart-ctl.md)  
[mx-dio-ctl](manuals/tools/mx-dio-ctl.md)  
[mx-led-ctl](manuals/tools/mx-led-ctl.md)  
[mx-relay-ctl](manuals/tools/mx-relay-ctl.md)  

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-it87.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Install required packages

```
yum distro-sync
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-it87.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`
  4. Notice: CentOS7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.

### Troubleshooting (CentOS 7.9)

- Device or resource busy when load gpio-it87 driver
  - Add `acpi_enforce_resources=lax` on boot parameter
  ```
  # Edit grub config
  vi /etc/default/grub
  GRUB_CMDLINE_LINUX="[...] acpi_enforce_resources=lax"
  
  # If you change this file, run 'grub2-mkconfig' afterwards to update
  grub2-mkconfig -o /etc/grub2.cfg
  grub2-mkconfig -o /etc/grub2-efi.cfg
  ```

- Enforce to use it87 series driver instead of iTCO driver (if necessary)
```
# edit `/lib/modprobe.d/iTCO-blacklist.conf`

blacklist iTCO_wdt
blacklist iTCO_vendor_support
```
