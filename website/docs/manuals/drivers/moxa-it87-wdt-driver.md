---
title: moxa-it87-wdt-driver
---

### Moxa IT87 Watchdog Timer Driver

`moxa-it87-wdt-driver`: Watchdog Timer Driver for ITE IT87xx Environment Control


#### Support Models

- DA Series: [DA-820C](manuals/products/DA/DA-820C.md)|[DA-682C](manuals/products/DA/DA-682C.md)|[DA-681C](manuals/products/DA/DA-681C.md)|[DA-720](manuals/products/DA/DA-720.md)
- MC Series: [MC-3201](manuals/products/MC/MC-3201.md)

#### Source Code Link

- For Kernel Version >= 5.x
  - [moxa-it87-wdt-driver (5.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/-/tree/5.2/master)

- For Kernel Version >= 4.x
  - [moxa-it87-wdt-driver (4.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/-/tree/4.9.x/master)

### What Tools Depend On?

Watchdog deamon from Linux Distributions

- Debian: https://packages.debian.org/bullseye/watchdog
- Ubuntu: https://manpages.ubuntu.com/manpages/focal/man8/watchdog.8.html
- CentOS 7: https://centos.pkgs.org/7/centos-x86_64/watchdog-5.13-12.el7.x86_64.rpm.html

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_wdt.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Install required packages

```
yum distro-sync
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_wdt.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`
  4. **Notice** CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
