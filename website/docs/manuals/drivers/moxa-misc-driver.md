---
title: moxa-misc-driver
---

## Moxa Misc Driver

The Moxa misc driver supported to access the relay/DI/DO, 
on board uart interface and programable LED control on DA-720/DA-682B, 
DA-682B-K6 MC-1100, MC-7400, MP-2070/2120/2121, ... series embedded computer.

The driver is based on GPIO sysfs, the Linux kernel shuld support thses features, CONFIG_GPIO_SYSFS=y and CONFIG_GPIOLIB=y.

### Support Models

- DA Series: [DA-720](manuals/products/DA/DA-720.md)

### Source Code Link

[moxa-misc-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-misc-drivers/-/tree/main)

### What Tools Depend On?

N/A

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Change the target model name in Makefile
- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `misc-moxa-<model_name>.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Install required packages

```
yum distro-sync
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```
- Change the target model name in Makefile
- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `misc-moxa-<model_name>.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### More Information

See Readme in [moxa-misc-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-misc-drivers/-/tree/main)
