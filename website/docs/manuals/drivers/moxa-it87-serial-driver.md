---
title: moxa-it87-serial-driver
---

## Moxa IT87 Serial Driver

`moxa-it87-serial-driver`:  
IT8786 support six standard serial ports and RS485 automatic direction control.  
This driver provide an interface under misc device for controlling serial register.

### Support Models

- DA Series: [DA-820C](manuals/products/DA/DA-820C.md)|[DA-682C](manuals/products/DA/DA-682C.md)|[DA-681C](manuals/products/DA/DA-681C.md)
- MC Series: [MC-3201](manuals/products/MC/MC-3201.md)


### Source Code Link

[moxa-it87-serial-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-serial-driver/-/tree/master)

### What Tools Depend On?

[mx-uart-ctl](manuals/tools/mx-uart-ctl.md)

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_serial.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Install required packages

```
yum distro-sync
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_serial.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`
  4. Notice: CentOS7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.

### Example after probing modules

- /sys/class/misc/it87_serial/

```
dev  power  serial1  serial2  subsystem  uevent
```

- /sys/class/misc/it87_serial/serial1

For initial version, provide an interface to control RS485 mode control.
```
serial1_rs485
```
