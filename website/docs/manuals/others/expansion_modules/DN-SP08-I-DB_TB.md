---
title: DN-SP08-I-DB/TB
---

8-port software-selectable RS-232/422/485 PCIe serial module with DB9/TB9 connector

## Datasheet

- [DN-SP08-I-DB/TB Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-682c-uart-series-expansion-modules)  
- [moxa-da-682c-uart-series-expansion-modules-datasheet-v1.0.pdf](https://cdn-cms.azureedge.net/getmedia/a01ebd25-4b9d-4ea2-9b1e-20dda8db64f6/moxa-da-682c-uart-series-expansion-modules-datasheet-v1.0.pdf)

## Prerequisite

See [Introduction](manuals/intro.md) page for more informations.

