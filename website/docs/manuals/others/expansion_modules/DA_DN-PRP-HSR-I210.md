---
title: DN/DA-PRP-HSR-I210
---

## Datasheet

### DN-PRP-HSR-I210

The DN-PRP-HSR-I210 expansion module is compliant with IEC 62439-3 Clause 4 (PRP) and IEC 62439-3 Clause 5 (HSR) to ensure the highest system availability and data integrity for mission-critical applications that require redundancy and zero-time recovery. With its dual Gigabit Ethernet port design, the DN-PRP-HSR-I210 module provides high performance for redundant network systems.

- [DN-PRP-HSR-I210 Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-682c-ethernet-series-expansion-modules/dn-prp-hsr-i210) 
- [moxa-da-682c-ethernet-series-expansion-modules-datasheet-v1.0.pdf](https://www.moxa.com/getmedia/8095b4c2-5917-43a1-827d-2c12fd0bd51b/moxa-da-682c-ethernet-series-expansion-modules-datasheet-v1.0.pdf)

### DA-PRP-HSR-I210

The DA-PRP-HSR-I210 expansion card model is compliant with IEC 62439-3 Clause 4 (PRP) and IEC 62439-3 Clause 5 (HSR) to ensure the highest system availability and data integrity for mission-critical applications that require redundancy and zero-time recovery. With a dual Gigabit Ethernet combo port design, the expansion card provides high performance for redundant network systems. In addition, it features a built-in native PRP/HSR management middleware with MMS server that allows SCADA to collect IEC62439-3 registers from multiple devices for easy network diagnosis, troubleshooting, device management, and monitoring.

- [DA-PRP-HSR-I210 Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-820c-ethernet-series-expansion-modules/da-prp-hsr-i210)  
- [moxa-da-820c-ethernet-series-expansion-modules-datasheet-v1.0.pdf](https://www.moxa.com/getmedia/523208f2-4e3b-4904-8bf3-f64481ec8c9e/moxa-da-820c-ethernet-series-expansion-modules-datasheet-v1.0.pdf)

## Prerequisite

See [Introduction](manuals/intro.md) page for more informations.
