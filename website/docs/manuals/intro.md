---
title: Introduction
---

The following tables provide the relationship with models, including supported drivers, tools and modules.

### Table of Drivers Support

List of Drivers:  
[moxa-it87-gpio-driver](manuals/drivers/moxa-it87-gpio-driver.md), [moxa-it87-serial-driver](manuals/drivers/moxa-it87-serial-driver.md), [moxa-it87-wdt-driver](manuals/drivers/moxa-it87-wdt-driver.md), [moxa-misc-driver](manuals/drivers/moxa-misc-driver.md)

| Models\Drivers | moxa-it87-gpio-driver | moxa-it87-serial-driver | moxa-it87-wdt-driver |  moxa-misc-driver  |
| -------------- | --------------------- | ----------------------- | -------------------- | ------------------ |
| MC-3201        |  :heavy_check_mark:   |   :heavy_check_mark:    |   :heavy_check_mark: |                    |
| DA-820C        |  :heavy_check_mark:   |   :heavy_check_mark:    |   :heavy_check_mark: |                    |
| DA-682C        |  :heavy_check_mark:   |   :heavy_check_mark:    |   :heavy_check_mark: |                    |
| DA-681C        |  :heavy_check_mark:   |   :heavy_check_mark:    |   :heavy_check_mark: |                    |
| DA-720         |                       |                         |   :heavy_check_mark: | :heavy_check_mark: |


### Table of Tools Support

List of Tools:  
[mx-uart-ctl](manuals/tools/mx-uart-ctl.md), [mx-dio-ctl](manuals/tools/mx-dio-ctl.md), [mx-led-ctl](manuals/tools/mx-led-ctl.md), [mx-relay-ctl](manuals/tools/mx-relay-ctl.md)


| Models\Tools     |    mx-uart-ctl     |    mx-dio-ctl      |    mx-led-ctl      |    mx-relay-ctl    |   moxa-misc-driver  |
| ---------------- | ------------------ | ------------------ | ------------------ | ------------------ | ------------------- |
| MC-3201          | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                     |
| DA-820C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                     |
| DA-682C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                     |
| DA-681C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                     |
| DA-720           |                    |                    |                    |                    |  :heavy_check_mark: |


### Table of Expansion Modules Support

List of Expansion Modules:  
- PRP-HSR-I210 Expansion Modules: [DA/DN-PRP-HSR-I210](manuals/others/expansion_modules/DA_DN-PRP-HSR-I210.md)  
- UART Expansion Modules: [DN-SP08-I-DB/TB](manuals/others/expansion_modules/DN-SP08-I-DB_TB.md)
- IRIGB Expansion Modules: [DA-IRIGB-B-S](manuals/others/expansion_modules/DA-IRIGB-B-S.md)

| Models\Tools     |   DA-PRP-HSR-I210  |  DN-PRP-HSR-I210   |  DN-SP08-I-DB/TB   |   DA-IRIGB-B-S     |
| ---------------- | ------------------ | ------------------ | ------------------ | ------------------ |
| DA-820C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| DA-682C          |                    | :heavy_check_mark: | :heavy_check_mark: |                    |
