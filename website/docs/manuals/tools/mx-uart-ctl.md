---
title: mx-uart-ctl
---

### Moxa UART Control

`mx-uart-ctl` is a common script for controlling UART's interface mode

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/UART/mx-uart-ctl)

### Support Model

- DA Series:
  - [DA-820C](#da-820c)
  - [DA-682C](#da-682c)
  - [DA-681C](#da-681c)
- MC Series:
  - [MC-3201](#mc-3201)
### Prerequisite
Before use `mx-uart-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](manuals/drivers/moxa-it87-gpio-driver.md)
2. [moxa-it87-serial-driver](manuals/drivers/moxa-it87-serial-driver.md)

### Usage
> mx-uart-ctl -p <port_number> [-m <uart_mode>]


- Get mode from port 0
 ```
 mx-uart-ctl -p 0
 ```
- Set port 1 to mode RS232
 ```
 mx-uart-ctl -p 1 -m 0
 ```
### Model IO Table
#### DA-820C
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

- 8 ports expansion module (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 2           | /dev/ttyUSB0 |
 | P2              | 3           | /dev/ttyUSB1 |
 | P3              | 4           | /dev/ttyUSB2 |
 | P4              | 5           | /dev/ttyUSB3 |
 | P5              | 6           | /dev/ttyUSB4 |
 | P6              | 7           | /dev/ttyUSB5 |
 | P7              | 8           | /dev/ttyUSB6 |
 | P8              | 9           | /dev/ttyUSB7 |

#### DA-682C
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

- When 1 UART expansion module is plugged-in (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 2           | /dev/ttyUSB0 |
 | P2              | 3           | /dev/ttyUSB1 |
 | P3              | 4           | /dev/ttyUSB2 |
 | P4              | 5           | /dev/ttyUSB3 |
 | P5              | 6           | /dev/ttyUSB4 |
 | P6              | 7           | /dev/ttyUSB5 |
 | P7              | 8           | /dev/ttyUSB6 |
 | P8              | 9           | /dev/ttyUSB7 |

- When 2 UART expansion modules are plugged-in (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node   |
 |-----------------|-------------|---------------|
 | P1              | 10          | /dev/ttyUSB8  |
 | P2              | 11          | /dev/ttyUSB9  |
 | P3              | 12          | /dev/ttyUSB10 |
 | P4              | 13          | /dev/ttyUSB11 |
 | P5              | 14          | /dev/ttyUSB12 |
 | P6              | 15          | /dev/ttyUSB13 |
 | P7              | 16          | /dev/ttyUSB14 |
 | P8              | 17          | /dev/ttyUSB15 |

#### DA-681C
- 12 ports on the rear panel

 | Panel Port Name | Port Number | Device Node   |
 | --------        | ----------- | ------------  |
 |    P1           | 0           | /dev/ttyUSB0  |
 |    P2           | 1           | /dev/ttyUSB1  |
 |    P3           | 2           | /dev/ttyUSB2  |
 |    P4           | 3           | /dev/ttyUSB3  |
 |    P5           | 4           | /dev/ttyUSB4  |
 |    P6           | 5           | /dev/ttyUSB5  |
 |    P7           | 6           | /dev/ttyUSB6  |
 |    P8           | 7           | /dev/ttyUSB7  |
 |    P9           | 8           | /dev/ttyUSB8  |
 |    P10          | 9           | /dev/ttyUSB9  |
 |    P11          | 10          | /dev/ttyUSB10 |
 |    P12          | 11          | /dev/ttyUSB11 |

#### MC-3201
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |