---
title: mx-relay-ctl
---

### Moxa Relay Control

`mx-relay-ctl` is a common script for controlling Relay's interface mode

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/RELAY/mx-relay-ctl)

### Support Model

- DA Series:
  - [DA-820C](#da-820c)
  - [DA-682C](#da-682c)
  - [DA-681C](#da-681c)

### Prerequisite
Before use `mx-relay-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](manuals/drivers/moxa-it87-gpio-driver.md)

### Usage
> mx-relay-ctl -p <port_number> [-m <relay_mode>]

- Get mode from port 0
 ```
 mx-relay-ctl -p 0
 ```

- Set port 0 to mode NC
 ```
 # mx-relay-ctl -p 0 -m 0
 ```

- Set port 0 to mode NO
 ```
 # mx-relay-ctl -p 0 -m 1
 ```

### Model IO Table
#### DA-820C
- 1 relay port on the rear panel

 | Relay Index |
 | --------  |
 | 0 |

#### DA-682C
- 1 relay port on the rear panel

 | Relay Index |
 | --------  |
 | 0 |

#### DA-681C
- 1 relay port on the rear panel

 | Relay Index |
 | --------  |
 | 0 |