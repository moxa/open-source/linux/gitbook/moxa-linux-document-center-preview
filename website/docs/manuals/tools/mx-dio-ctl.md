---
title: mx-dio-ctl
---

### Moxa DIO Control

`mx-dio-ctl` is a common script for controlling DIO's interface mode

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/DIO/mx-dio-ctl)

### Support Model

- DA Series:
  - [DA-820C](#da-820c)
  - [DA-682C](#da-682c)
  - [DA-681C](#da-681c)
- MC Series:
  - [MC-3201](#mc-3201)

### Prerequisite
Before use `mx-dio-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](manuals/drivers/moxa-it87-gpio-driver.md)

### Usage
> mx-dio-ctl <-i|-o <#port number> [-s <#state>]>

- Get value from DIN port 0
 ```
 mx-dio-ctl -i 0
 ```
- Get value from DOUT port 0
  ```
  mx-dio-ctl -o 0
  ```
- Set DOUT port 0 value to LOW
  ```
  mx-dio-ctl -o 0 -s 0
  ```
- Set DOUT port 0 value to HIGH
  ```
  mx-dio-ctl -o 0 -s 1
  ```

### Model IO Table
#### DA-820C
- 6 DIs and 2 DOs on the rear panel

 | DIO | Port Number |
 |-----|-------------|
 | DI  | 0           |
 | DI  | 1           |
 | DI  | 2           |
 | DI  | 3           |
 | DI  | 4           |
 | DI  | 5           |
 | DO  | 0           |
 | DO  | 1           |

#### DA-682C
- 6 DIs and 2 DOs on the rear panel

 | DIO | Port Number |
 |-----|-------------|
 | DI  | 0           |
 | DI  | 1           |
 | DI  | 2           |
 | DI  | 3           |
 | DI  | 4           |
 | DI  | 5           |
 | DO  | 0           |
 | DO  | 1           |

#### DA-681C
- 6 DIs and 2 DOs on the rear panel

 | DIO | Port Number |
 |-----|-------------|
 | DI  | 0           |
 | DI  | 1           |
 | DI  | 2           |
 | DI  | 3           |
 | DI  | 4           |
 | DI  | 5           |
 | DO  | 0           |
 | DO  | 1           |

#### MC-3201
