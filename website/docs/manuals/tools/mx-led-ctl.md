---
title: mx-led-ctl
---

### Moxa LED Controll

`mx-led-ctl` is a common script for controlling LED's interface mode

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/LED/mx-led-ctl)

### Support Model

- DA Series:
  - [DA-820C](#da-820c)
  - [DA-682C](#da-682c)
  - [DA-681C](#da-681c)

### Prerequisite
Before use `mx-led-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](manuals/drivers/moxa-it87-gpio-driver.md)

### Usage
> mx-led-ctl -i <led_index> [on|off]

- Get state from index 1
 ```
 mx-led-ctl -i 1
 ```

- Set index 1 to on
 ```
 mx-led-ctl -i 1 on
 ```

### Model IO Table
#### DA-820C
- 8 programmable LEDs on the front panel

 | LED Index |
 | --------  |
 | 1 |
 | 2 |
 | 3 |
 | 4 |
 | 5 |
 | 6 |
 | 7 |
 | 8 |

#### DA-682C
- 8 programmable LEDs on the front panel

 | LED Index |
 | --------  |
 | 1 |
 | 2 |
 | 3 |
 | 4 |
 | 5 |
 | 6 |
 | 7 |
 | 8 |

#### DA-681C
- 8 programmable LEDs on the front panel

 | LED Index |
 | --------  |
 | 1 |
 | 2 |
 | 3 |
 | 4 |
 | 5 |
 | 6 |
 | 7 |
 | 8 |