---
title: DA-682C
---

## Datasheet

- [DA-682C Series Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-682c-series)  
- [moxa-da-682c-series-datasheet-v1.5.pdf](https://cdn-cms.azureedge.net/getmedia/caa08958-1e46-47df-8898-e6886ea88c14/moxa-da-682c-series-datasheet-v1.5.pdf)

## Prerequisite

See [Introduction](manuals/intro.md) page to check necessary drivers/tools/modules informations.
