/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Moxa Linux Document Center',
  tagline: '',
  url: 'https://moxa.gitlab.io',
  baseUrl: '/open-source/linux/gitbook/moxa-linux-document-center-preview/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/moxa_favicon.png',
  organizationName: 'Moxa', // Usually your GitHub org/user name.
  projectName: 'moxa-linux-document-center', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Moxa Linux Document Center',
      logo: {
        alt: 'Moxa Industrial Linux Logo',
        src: 'img/MIL_logo_1.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'MIL/what-is-MIL',
          label: 'Moxa Industrial Linux',
          position: 'left',
        },
        {
          type: 'doc',
          docId: 'manuals/what-is-x86-Linux-drivers-SDK',
          label: 'x86 Linux drivers & SDK',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Moxa Industrial Linux',
              to: '/',
            },
            {
              label: 'x86 Linux drivers & SDK',
              to: '/manuals/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Moxa Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
        },
        pages: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
