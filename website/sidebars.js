module.exports = {
  releasenote: [
    'MIL/what-is-MIL',
    {
      type: 'category',
      collapsed: false,
      label: 'Release Note',
      items: [
        {
          type: 'category',
          label: 'MIL 1.1.0',
          collapsed: false,
          items: [
            'MIL/MIL110/MIL110_UC-2100_UC-2100-W_ReleaseNote',
            'MIL/MIL110/MIL110_UC-3100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-5100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100A-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8200_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8410A_ReleaseNote',
          ],
        },
      ],
    },
  ],
  manuals: [
    'manuals/what-is-x86-Linux-drivers-SDK',
    {
      type: 'category',
      label: 'x86 Linux drivers & SDK',
      collapsed: false,
      items: [
        'manuals/intro',
        {
          type: 'category',
          label: 'Drivers',
          collapsed: false,
          items: [
            'manuals/drivers/moxa-it87-gpio-driver',
            'manuals/drivers/moxa-it87-wdt-driver',
            'manuals/drivers/moxa-it87-serial-driver',
            'manuals/drivers/moxa-misc-driver'
         ],
        },
        {
          type: 'category',
          label: 'Tools',
          collapsed: false,
          items: [
            'manuals/tools/mx-uart-ctl',
            'manuals/tools/mx-dio-ctl',
            'manuals/tools/mx-led-ctl',
            'manuals/tools/mx-relay-ctl'
         ],
        },
        {
          type: 'category',
          label: 'Others',
          collapsed: false,
          items: [
            {
              type: 'category',
              label: 'Wireless',
              items: [
                {
                  type: 'category',
                  label: 'WiFi Module Usage',
                  items: [
                     'manuals/others/wireless/wifi/SparkLAN-WPEQ-261ANCI-BT'
                  ],
                },
                {
                  type: 'category',
                  label: 'Cellular Module Usage',
                  items: [
                     'manuals/others/wireless/cellular/Telit-LE910C4'
                  ],
                },
              ],
            },
            {
              type: 'category',
              label: 'Expansion Modules',
              items: [
                 'manuals/others/expansion_modules/DA_DN-PRP-HSR-I210',
                 'manuals/others/expansion_modules/DN-SP08-I-DB_TB',
                 'manuals/others/expansion_modules/DA-IRIGB-B-S',
              ],
            },
          ],
        },
        {
          type: 'category',
          label: 'Prodcucts Q&A',
          collapsed: false,
          items: [
            'manuals/products/DA/DA-820C',
            'manuals/products/DA/DA-682C',
            'manuals/products/DA/DA-681C',
            'manuals/products/DA/DA-720',
            'manuals/products/MC/MC-3201'
          ],
        },
        {
          type: 'category',
          label: 'Release Note',
          collapsed: false,
          items: [
            'manuals/releasenote/V1.0',
         ],
        },
      ],
    },
  ],
};
